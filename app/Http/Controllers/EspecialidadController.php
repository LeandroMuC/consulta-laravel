<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Especialidad;
class EspecialidadController extends Controller
{
    public function index(){
        $data=Especialidad::all();
        return view('web.especialidad.index')->with('data',$data);
    }
}
