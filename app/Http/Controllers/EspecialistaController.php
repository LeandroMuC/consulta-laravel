<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Especialista;

class EspecialistaController extends Controller
{
    public function index(){
        $data=Especialista::all();
        return view('web.Especialista.index')->with('data',$data);
    }
}
