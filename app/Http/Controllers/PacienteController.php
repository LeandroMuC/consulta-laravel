<?php

namespace App\Http\Controllers;
use App\Paciente;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    public function index(){
        $data=Paciente::all();
        return view('web.paciente.index')->with('data',$data);
    }
}
