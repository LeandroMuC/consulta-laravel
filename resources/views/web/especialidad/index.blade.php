@extends('web.common.base')
@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                @if(session('status'))
            <div class="alert alert-success">
            {{session('status')}}
            </div>
            @endif
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Listado de Examen</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                                  <ul class="dropdown-menu dropdown-user">
                                <li><a href="/especialidad/">Agregar Nuevo Especialista</a>
                                </li>
                                <!--<li><a href="#">Config option 2</a>
                                </li>-->
                            </ul>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                    <table id="index_table" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <th>id</th>
                        <th>Descripcion</th>                        
                        <th>Enable</th>
                        <th></th>                                            
                    </thead>            
                    @foreach($data as $dat)        
                    <tr>
                        <td>{{$dat->id}}</td>
                        <td>{{$dat->descripcion}}</td>
                        <td>{{$dat->enable }}</td>                        
                        <td><a href="/especialidad/{{$dat->id}}" class="btn btn-success"><span class="fa fa-edit"></span></a>
                        <a href="/especialidad/eliminar/{{$dat->id}}" class="btn btn-danger"><span class="fa fa-trash"></span></a>
                        </td>                  
                    </tr>
                    @endforeach
                    </tfoot>
                    </table>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
<script type="text/javascript">
    var data=[];
    $(document).ready(function(){
 
        oTable = $('#index_table').dataTable({         
       
        });
       
    });
</script>		 
@endsection