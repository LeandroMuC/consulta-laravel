@extends('web.common.base')
@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class=" ibox_title">
                    <h5>Ingresar Datos</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <form action="/examen" method="POST">
                    {{ method_field('PUT') }
                    <div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="Descripcion">(*)Descripcion</label>
                                <div class="col-md-4">
                                    <input class="form-control Descripcion required entry mayuscula" id="Descripcion" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="Preparacion">(*)Preparacion</label>
                                <div class="col-md-4">
                                    <input class="form-control  entry mayuscula" id="Preparacion" type="text" />
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn-success btn-lg" id="guardar" disabled="disabled">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function validar_campos(contenedor) {
            var $contenedor = $(contenedor);
            var exit = false;
            $contenedor.find("input[type=text],input[type=radio],input[type=checkbox],select,textarea").each(function (id, obj) {
                var a = $(obj).attr("id");
                var class_name = $('#id_span_' + a.toLocaleLowerCase()).attr("class");
                if (class_name != undefined && class_name != "") {
                    exit = true;
                }
                if ($(obj).val() == "" || $(obj).val() == undefined) {
                    $(obj).attr("style", "background:#fff no-repeat 98% center; box-shadow:0 0 5px #d45252; border-color:#b03535");
                    message = "verifique los campos Nulos"
                    exit = true;
                } else {
                    $(obj).attr("style", "background:#fff no-repeat 98% center; box-shadow:0 0 5px #5cd053; border-color:#28921f");
                }
            });
            if (!exit) {
                $('#guardar').removeAttr("disabled");
            } else {
                $('#guardar').attr("disabled", "disabled");
            }

    }
    $(document).on("keyup", ".mayuscula", function () {
         $(this).val($(this).val().toUpperCase());
     });
     $(document).on("blur", ".entry", function () {
         console.log("entro aqui");
         validar_campos("#encabezado");
     });
</script>
@endsection